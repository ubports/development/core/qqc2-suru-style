2024-10-11 Mike Gabriel

        * Release 0.20230630 (HEAD -> main, tag: 0.20230630)

2023-06-30 Marius Gripsgard

        * Merge branch 'revert-b31c1c8e' into 'main' (c0cf200)

2023-06-24 Maciej Sopyło

        * Revert "Merge branch 'feat/live-switch' into 'main'" (aa5d8cd)

2023-06-12 Mike Gabriel

        * Merge branch 'personal/peat-psuwit/fix-per-screen-gu' into 'main'
          (421417b)

2023-06-11 Ratchanan Srirattanamet

        * QQuickSuruUnits: mimic QHighDpiScaling in how to get the factor
          (d41acb2)

2023-06-06 Mike Gabriel

        * Merge branch 'personal/fredldotme/qqc2hidpi' into 'main' (d81ff98)

2022-11-30 Alfred Neumayer

        * qquicksuru: Scale units based on the current screen (9c93f0b)

2023-02-06 Mike Gabriel

        * Release 0.20230206 (6b84620) (tag: 0.20230206)

2023-01-19 Mike Gabriel

        * Merge branch 'qt6-support' into 'main' (aefd4a7)

2023-01-18 Rodney Dawes

        * Enable building against both Qt5 and Qt6 (6c82553)

2023-01-17 Mike Gabriel

        * Merge branch 'feat/live-switch' into 'main' (b31c1c8)

2023-01-15 Maciej Sopylo

        * Enable live switching when theme file changes (cb68f8a)

2023-01-15 Mike Gabriel

        * Merge branch 'focal-fix-suru-dark' into 'main' (e38d86f)

2023-01-15 Maciej Sopyło

        * Fixed SuruDark detection on focal (8ba2301)

2022-06-07 Florian Leeber

        * Merge branch 'qt5.12-imports' into 'main' (b4c0f34)

2022-06-07 Guido Berhoerster

        * Merge branch 'packaging-cleanup' into 'main' (15c1884)

2022-06-06 Alberto Mardegan

        * Update QML imports for Qt 5.12 (73a23b3)

2022-05-27 Guido Berhoerster

        * Release new version (15b9e1e)
        * Update Maintainer, Homepage, VCS URLs (0e9d45f)
        * Move Jenkinsfile to debian/ (89db63b)

2021-11-23 Daniel Kutka

        * Add support for icons and 5.12 fixes (#56) (428670b)

2021-02-02 Rodney

        * Merge pull request #54 from ubports/xenial_-_fix-qmldir (ab4a8de)

2021-01-27 Rodney Dawes

        * Move the internal implementation items to impl subdir (fb257d5)

2021-01-12 Brian Douglass

        * Merge pull request #52 from ubports/xenial_-_qt-5-12 (961b601)

2021-01-11 Rodney Dawes

        * Update Jenkinsfile (b7fd5e1)

2020-09-05 dano6

        * Fix ComboBox for 5.12 (dd92a6f)

2020-09-01 dano6

        * Port CheckIndicator and SpinBox to QT 5.12 (3d7f6a4)

2020-09-01 Rodney Dawes

        * Add missing QtQuick.Controls 2 import (54f7dbc)

2020-09-28 Marius Gripsgard

        * Merge pull request #50 from mardy/crossbuilder (6d9fd26)

2020-09-26 Alberto Mardegan

        * debian/rules: do not override existing QT_SELECT value (443c00c)

2020-06-16 Brian Douglass

        * Merge pull request #45 from ubports/fix-editable-combo-box
          (4bd56af)

2020-05-28 Brian Douglass

        * Added nasty hack to allow clickable to work again (b4ef28c)
        * Fixed the combo box's editable state (4e8368a)

2020-01-26 Johannes Renkl

        * Removed type registration of Label (#41) (5142b36)

2020-01-26 Luca Weiss

        * Fix missing .qml files in Qt 5.9 (#40) (4c3b6cf)

2020-01-24 Luca Weiss

        * Port C++ parts to Qt 5.12 (#38) (f1cd481)

2019-12-11 Brian Douglass

        * Merge pull request #37 from cibersheep/xenial (c7b04bf)

2019-12-11 Joan CiberSheep

        * Set Elevation shadow to grey (b58b154)

2019-12-11 Brian Douglass

        * Merge pull request #36 from cibersheep/xenial (6a50e44)

2019-12-10 Joan CiberSheep

        * Fix to Sliders handler light and dark mode (8894516)

2019-12-06 Brian Douglass

        * Merge pull request #35 from balcy/xenial (1f0da96)

2019-12-02 Chris Clime

        * add foregroundColor to SpinBox (5160595)

2019-08-25 Brian Douglass

        * Merge pull request #33 from ubports/xenial_-_label-wrap (89707e3)

2019-08-23 Brian Douglass

        * Follow upstream's default for word wrap (9d27650)

2019-05-30 Joan CiberSheep

        * Merge pull request #30 from ubports/xenial_-_fix-dial (0f38e67)

2019-05-30 Brian Douglass

        * Set the default dial background width to match the material design
          size (0eb59ca)

2019-05-09 Joan CiberSheep

        * Merge pull request #27 from ubports/xenial_-_fix-label (cc061ff)

2019-05-09 Brian Douglass

        * Removed problematic width setting in the Label element (ff72403)

2019-05-05 Brian Douglass

        * Fixed dp unit when HighDpiScaling is active (753661b)

2019-04-19 Brian Douglass

        * Merge pull request #24 from
          hummlbach/secondary_tertiary_foreground_color (d3982dc)

2019-04-19 Johannes Renkl

        * Added tertiaryForegroundColor and defined the foreground colors
          once (6204080)

2019-04-16 Michele

        * added secondaryForegroundColor (a05508f)

2019-04-01 Brian Douglass

        * Merge pull request #21 from ubports/Revert#15 (ce1056b)

2019-04-01 Joan CiberSheep

        * Restore Page background (20dfb98)

2019-03-26 Brian Douglass

        * Merge pull request #17 from ubports/SVGspinner (0421060)
        * Fixed the size and color of the svg spinner (5f06296)

2019-03-21 Joan CiberSheep

        * Add Scalable Spinner Icon (8f449db)

2019-03-20 Brian Douglass

        * Merge branch 'master' into xenial (f9e5df0)
        * Merge pull request #16 from ubports/SubtileTransitions (8def0e9)
        * Merge pull request #15 from ubports/NoPageBackground (efc8dfc)

2019-03-20 Joan CiberSheep

        * Simpler Transitions for StackView (b756125)
        * Remove Page background (bd803c9)

2019-03-12 Joan CiberSheep

        * Merge pull request #14 from ubports/xenial_-_respect-grid-unit
          (93d81ca)

2019-03-12 Brian Douglass

        * Merge pull request #13 from ubports/xenial_-_dark-theme-detection
          (a829d0d)
        * Respect the GRID_UNIT_PX env var (5a96fb5)

2019-03-10 Brian Douglass

        * Merge branch 'xenial' into xenial_-_dark-theme-detection (d816355)

2019-02-22 Brian Douglass

        * Added clickable setup for easier testing on device (8964f4f)
        * Added detection for the Ubuntu Touch dark theme (7c69328)

2018-05-18 Marius Gripsgard

        * Trigger rebuild (f7cb0a5)

2018-05-05 Marius Gripsgard

        * Merge pull request #3 from JBBgameich/patch-1 (37c2a31)

2018-05-04 JBB

        * Update control (6108356)
        * debian: Add qtdeclarative5-private-dev and qtdeclarative5-dev-tools
          deps (2c11e92)
        * debian: Add missing pkg-kde-tools dependency (a357f0d)

2018-05-04 Dan Chapman

        * Update debian/rules (d35f117)
        * Update control file (d2327c7)
        * Update changelog (ae9901d)
        * Add jenkins file (6b79168)

2018-02-23 Stefano Verzegnassi

        * Merge pull request #2 from JBBgameich/master (be8137a)

2018-02-23 JBBgameich

        * Add debian packaging (2449552)

2018-01-31 Stefano Verzegnassi

        * Use better 'maxWidth' values for Label (04760bb)

2018-01-30 Stefano Verzegnassi

        * Use 'backgroundColor' in Frame (d543ff0)
        * Added new 'textLevel' and 'textStyle' properties. Removed
          'secondaryForegroundColor', as it's not necessary anymore
          (01d3f90)
        * Removed elevation effect from ToolBar (dad2975)
        * Added QQuickSuruUnits object. Use 'gu' and 'dp' for sizes.
          (01ee8f1)

2018-01-27 Stefano Verzegnassi

        * Fixed unreadable colors (aa10956)
        * Add not-commited 'EasingOutIn' property (c49645a)
        * Refactored animations (dc137fd)

2018-01-26 Stefano Verzegnassi

        * Refactored style engine (dea658d)

2018-01-25 Stefano Verzegnassi

        * removed comments (d6fbc69)

2018-01-23 Stefano Verzegnassi

        * Merge pull request #1 from sverzegnassi/new_elevation_effect
          (eb63123)

2018-01-22 Stefano Verzegnassi

        * New elevation effect, based on Suru specs (713d651)
        * [ProgressBar] Refreshed contentItem visuals. Removed Material style
          import. (2cf1784)

2018-01-19 Stefano Verzegnassi

        * Added missing DelayButton component (42b70f7)
        * clean up (38a3e59)
        * Fixed activeFocus visuals (ab37d82)
        * Restore 'quick-private' (fe6b99a)
        * [TEST] Toolbar: add elevation effect (cc409b0)
        * ComboBox: use same 'radius' value for both control and popup
          (672f4e6)
        * Indicators: use 'backgroundColor' for bg (467e45f)
        * Removed unused QT_PRIVATE imports (5d2fa58)
        * Removed QQuickSuruHighlightRectangle, replaced by a QML component
          (de55a84)
        * Button: use ElevationEffect (a34b670)

2018-01-18 Stefano Verzegnassi

        * Fixed typo in QQuickSuruHighlightRectangle (8256ae7)
        * Enlarged Menu radius + added some padding to prevent MenuItem
          overlapping (de834ec)
        * Tumbler visuals (7c1fdc3)
        * clean up (c8da191)
        * Removed quicktemplates2-private dependency (20bd06c)
        * Use positiveColor for indicators 'checked' state (7b9a4fd)
        * SwitchIndicator a11y (5904a96)
        * Replaced blue with link-color from Vanilla Framework. Ref.
          https://github.com/ubuntudesign/vanilla-design/commit/5d6c3f3db27e4bc1ca598996d70880a3119bf732
          (e94cff3)

2018-01-17 Stefano Verzegnassi

        * Initial commit (83afea5)
